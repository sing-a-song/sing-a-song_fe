import axios from 'axios';
import { defineStore } from 'pinia';

import type { SongInsert, PaginationSongs } from '@/types/Song';
import type { User, UserLogin, UserSignup } from '@/types/User';

export const useStore = defineStore('main', {
  state: () => ({
    paginationSongs: null as PaginationSongs | null,
    token: null as String | null,
    user: null as User | null,
    
    currentPage: 1,
  }),
  getters: {
    songs(state) {
      return state.paginationSongs?.data;
    },
    pagination(state) {
      return {
        next: state.paginationSongs?.pagination.next,
        previeous: state.paginationSongs?.pagination.previous,
        searchQuery: state.paginationSongs?.searchQuery,
        count: state.paginationSongs?.count,
        total: state.paginationSongs?.total,
      };
    },
  },
  actions: {
    async fetchSongs(queryParams: string) {
      await axios
        .get(`http://localhost:8000${queryParams}`)
        .then((response) => {
          this.paginationSongs = {
            data: response.data.data,
            pagination: response.data.pagination,
            searchQuery: response.data.search_query,
            total: response.data.total,
            count: response.data.count,
          };
        })
        .catch((error) => {
          console.log(`Error while fetching data... ${error}`);
        });
    },
    async insertSong(payload: SongInsert) {
      await axios
        .post('http://localhost:8000/songs/', payload, {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${this.token}`,
          },
        })
        .then((response) => {
          console.log(response.data);
        })
        .catch((error) => {
          console.error(error);
        });
    },
    async signUp(payload: UserSignup) {
      await axios
        .post(
          'http://localhost:8000/users/signup',
          {
            username: payload.username,
            email: payload.email,
            full_name: payload.fullName,
            password: payload.password,
          },
          {
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }
        )
        .catch((error) => {
          console.log(error);
        });

      this.logIn({
        password: payload.password,
        username: payload.username,
      });
    },
    async logIn(payload: UserLogin) {
      await axios
        .post(
          'http://localhost:8000/users/login/',
          new URLSearchParams({
            username: payload.username,
            password: payload.password,
          }),
          {
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          }
        )
        .then((response) => {
          this.token = response.data.access_token;
        })
        .catch((error) => {
          console.log(error);
        });

      await axios
        .get('http://localhost:8000/users/me/', {
          headers: {
            Accept: 'application/json',
            Authorization: `Bearer ${this.token}`,
          },
        })
        .then((response) => {
          console.log(response.data);
          this.user = {
            id: response.data.id,
            username: response.data.username,
            email: response.data.email,
            fullName: response.data.full_name,
          };
        })
        .catch((error) => {
          console.log(error);
        });
    },
    async testLogin() {
      await axios
        .get('http://localhost:8000/users/me/items/', {
          headers: {
            Accept: 'application/json',
            Authorization: `Bearer ${this.token}`,
          },
        })
        .then((response) => {
          console.log(response);
        })
        .catch((error) => {
          console.log(error.message);
        });
    },
    logOut() {
      this.token = null;
      this.user = null;
    },
  },
});
