import { type Comment } from "./Comment";

export interface Song {
  _id: string,
  title: string,
  artist: string,
  rating: number,
  timesRated: number,
  difficulty?: number,
  authorId: string,
  authorUsername: string,
  inserted_at: Date
}

export interface SongDetail {
  _id: string,
  title: string,
  artist: string,
  text: string
  rating: number,
  timesRated: number,
  difficulty?: number,
  capo?: number,
  tuning?: string[],
  key?: string,
  chords: string[],
  authorId: string,
  authorUsername: string,
  inserted_at: Date,
  comments: Comment[]
};

export interface SongInsert {
  title: string,
  artist: string,
  text: string
  difficulty?: number,
  capo?: number,
  tuning?: string[],
  key?: string,
  chords: string[]
}

interface Pagination {
  next: string | null,
  previous: string | null
}

export interface PaginationSongs {
  data: Song[],
  total: number,
  count: number,
  searchQuery: string,
  pagination: Pagination
}