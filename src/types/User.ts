export interface User {
  id: string,
  username: string,
  email: string,
  fullName: string
}

export interface UserLogin {
  username: string,
  password: string
}

export interface UserSignup extends UserLogin{
  email: string,
  fullName: string,
}