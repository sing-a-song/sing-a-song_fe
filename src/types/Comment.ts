export interface Comment {
  id: string,
  authorId: string,
  authorUsername: string,
  text: string,
  likes: number,
  likedBy: string[],
  insertedAt: Date
};
